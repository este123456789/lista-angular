import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';


import { HttpClientModule } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { FilmService } from './services/film.service';
import { LayoutComponent } from './_layuot/layout/layout.component';
import { AsideComponent } from './_layuot/aside/aside.component';
import { MainComponent } from './_layuot/main/main.component';
import { SidebarModule } from 'primeng/sidebar';

import { Ng2SmartTableModule } from '../../node_modules/ng2-smart-table';

import {APP_BASE_HREF} from '@angular/common';
import { ProfileComponent } from './profile/profile.component';  
import {FileUploadModule} from 'primeng/fileupload';
import { RegisterComponent } from './register/register.component';
import {CalendarModule} from 'primeng/calendar';


const route : Routes = [
{
  path: '',
  component: LayoutComponent,
  children:  [
    {path:'',component:MainComponent,pathMatch:'full'},
    {path:'profile',component:ProfileComponent},
    {path:'register',component:RegisterComponent}
  ]
},
{
  path:'**',
  redirectTo:''
}

];


@NgModule({
  declarations: [
  AppComponent,
  FilmsComponent,
  LayoutComponent,
  AsideComponent,
  MainComponent,
  ProfileComponent,
  RegisterComponent
  ],
  imports: [
  BrowserModule,
  HttpClientModule,
  RouterModule.forRoot(route),
  SidebarModule,
  Ng2SmartTableModule,
  BrowserAnimationsModule,
  FileUploadModule,
  CalendarModule
  ],
  providers: [FilmService,{provide: APP_BASE_HREF,useValue:'/'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
