import { Component, OnInit } from '@angular/core';
import { FilmService } from '../services/film.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Film } from './film';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

films: Film[];
data: Film[];
settings = {
	actions:false,
    columns: {
      id: {
        title: 'Id'
      },
      title: {
      	title: 'Title'
      },
  /*
      description: {
        title: 'Description'
      },
      */
      director: {
        title: 'Director'
      },
      /*
      locations:{
      	title: 'Locatios'
      },
      people: {
      	title: 'People'
      },
      species: {
      	title: 'Species'
      }
      */
      producer: {
      	title: 'Producer'
      },
      release_date: {
      	title: 'Release'
      },
      rt_score: {
      	title: 'Score'
      },


    }
  };

  constructor(private filmService:FilmService
  	) { }

  ngOnInit() {

  this.getAllFilms()
  	
  }

  getAllFilms(): void {

  	 this.filmService.getAllFilms().subscribe(
      films => {
		this.films =  films
      	console.log(this.films)
      }
      );
    }




}
