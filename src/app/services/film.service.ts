import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  urlAllFilms =  'https://ghibliapi.herokuapp.com/films'

  constructor(private http:HttpClient) { }

  getAllFilms():Observable<any> {
	const headers  = new HttpHeaders({'Content-Type':'application/json'});  	
	return this.http.get(this.urlAllFilms,{headers:headers});
  }
}
